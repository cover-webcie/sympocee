<?php
set_include_path ( dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' );
require_once 'include/init.php';
require_once 'include/forms/ParticipantForm.class.php';

/** Renders and processes Participant form */
class RegistrationView extends FormView
{
    protected $model;
    protected $template_base_name = 'templates/registration/registration';

    public function __construct(){
        parent::__construct('registration', 'Tickets');
        $this->model = get_model('Participant');
    }

    /** Returns the default context */
    protected function get_default_context() {
        return array_merge(parent::get_default_context(), [
            'symposium_full' => defined('MAX_PARTICIPANTS') && intval($this->model->count(['status__eq' => 'registered'])) >= MAX_PARTICIPANTS,
        ]);
    }

    /** Creates and returns the request form */
    protected function get_form() {
        $form = new ParticipantForm('signup');


        // Prefill form with data from the website if user is logged in.
        if (cover_session_logged_in()){
            $member = get_cover_session();
            $form->populate_fields([
                'first_name' => $member->voornaam,
                'surname' => empty($member->tussenvoegsel) ? $member->achternaam : $member->tussenvoegsel . ' ' . $member->achternaam,
                'birthday' => $member->geboortedatum,
                'address' => $member->adres,
                'postal_code' => $member->postcode,
                'city' => $member->woonplaats,
                'email' => $member->email,
                'phone' => $member->telefoonnummer
            ]);
        }

        return $form;
    }

    /** Renders response indicating whether the valid form was successfully processed (or not) */
    protected function form_valid($form){
        try {
            $data = $this->process_form_data($form->get_values());
            $context = ['status' =>  'success', 'data' => $data];
        } catch (Exception $e) {
            $context = [
                'status' => 'error', 
                'message' => $e->getMessage()
            ];
        }
        return $this->render_template($this->get_template('form_processed'), $context);
    }
    
    /** Processes the data of a valid form */
    protected function process_form_data($data) {
        // Create UUID
        $data['uuid'] = uniqid();

        // Add Cover ID if applicable
        if (cover_session_logged_in())
            $data['cover_id'] = get_cover_session()->id;

        // Convert booleans to tinyints
        $data['wants_expo'] = empty($data['wants_expo']) ? 0 : 1;
        $data['wants_membership'] = empty($data['wants_membership']) ? 0 : 1;
        $data['has_agreed_terms'] = empty($data['has_agreed_terms']) ? 0 : 1;

        if (defined('MAX_PARTICIPANTS') && 
                intval($this->model->count(['status__eq' => 'registered'])) >= MAX_PARTICIPANTS )
            $data['status'] = 'waiting_list';
        else
            $data['status'] = 'registered';

        // Create data
        $participant_id = $this->model->create($data);

        $data['qr'] = sprintf(
            'https://api.qrserver.com/v1/create-qr-code/?data=%s&size=200x200&qzone=4',
            urlencode(SERVER_NAME . '/registration/participant.php?uuid=' . $data['uuid'])
        );

        // Send confirmation email
        $success = send_mail(
            ADMIN_EMAIL,
            filter_var($data['email'], FILTER_SANITIZE_EMAIL),
            $this->render_template($this->get_template('email'), $data)
        );

        // Determine wether email has ben send succesfully
        if (!$success)
            throw new HttpException(500, 'Your registration has been stored in our database, but we failed to send you a confirmation email!');

        return $data;
    }
}


// Create and run home view
if (defined('REGISTRATIONS_OPEN') && !REGISTRATIONS_OPEN)
    $view = new TemplateView('registration/registration_closed');
else
    $view = new RegistrationView();

$view->run();
