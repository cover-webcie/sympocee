--
-- Table structure for table `participant`
--

-- TODO: Adjust fields to your liking
DROP TABLE IF EXISTS `participant`;
CREATE TABLE `participant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255), 
  `cover_id` int(11),
  `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `first_name` varchar(255),
  `surname` varchar(255),
  `address` varchar(255),
  `postal_code` varchar(255),
  `city` varchar(255),
  `email` varchar(254), -- official max length of an email-address
  `phone` varchar(100),
  `iban` varchar(34), -- official max length of an iban
  `bic` varchar(11), -- official max length of a bic
  `study` enum('Artificial Intelligence (BSc)','Artificial Intelligence (MSc)','Computing Science (BSc)','Computing Science (MSc)','Human Machine Communication (MSc)', 'Other') NOT NULL,
  `dinner` enum('dinner_1a','dinner_1b','dinner_1c','dinner_2a','dinner_2b','dinner_2c','dinner_3a','dinner_3b','dinner_3c','dinner_4') NOT NULL,
  `diet` enum('regular','vegetarian','vegan','other') NOT NULL,
  `remarks` varchar(1024),
  `has_agreed_terms` tinyint(1) NOT NULL DEFAULT '0',
  `wants_membership` tinyint(1) NOT NULL DEFAULT '0',
  `wants_expo` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('registered','cancelled','waiting_list') NOT NULL DEFAULT 'registered',
  PRIMARY KEY (`id`),
  UNIQUE (`email`),
  UNIQUE (`uuid`)
) ENGINE = INNODB;


--
-- Table structure for table `comment`
--

-- TODO: Adjust fields to your liking
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `participant_id` int(11) NOT NULL,
  `commenter_cover_id` int(11) NOT NULL,
  `commenter_name` varchar(255),
  `timestamp` DATETIME DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(255),
  `comment` varchar(4096),
  PRIMARY KEY (`id`),
  FOREIGN KEY fk_registration_user (participant_id)
    REFERENCES participant (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE = INNODB;
