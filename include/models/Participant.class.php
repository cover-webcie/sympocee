<?php
require_once 'include/models/Model.class.php';

class Participant extends Model
{
    // TODO: Adjust to your liking. Also update migrations if you change things.
    public static $study_options = ['Artificial Intelligence (BSc)','Artificial Intelligence (MSc)','Computing Science (BSc)','Computing Science (MSc)','Human Machine Communication (MSc)', 'Other'];
    public static $dinner_options = [
        'dinner_1a' => 'Dinner option 1a',
        'dinner_1b' => 'Dinner option 1b',
        'dinner_1c' => 'Dinner option 1c',
        'dinner_2a' => 'Dinner option 2a',
        'dinner_2b' => 'Dinner option 2b',
        'dinner_2c' => 'Dinner option 2c',
        'dinner_3a' => 'Dinner option 3a',
        'dinner_3b' => 'Dinner option 3b',
        'dinner_3c' => 'Dinner option 2c',
        'dinner_4' => 'I don\'t want lunch',
    ];
    public static $diet_options = [
        'regular' => 'Regular',
        'vegetarian' => 'Vegetarian',
        'vegan' => 'Vegan',
        'other' => 'Other (see comments)',
    ];
    public static $status_options = ['registered','cancelled','waiting_list'];

    public function __construct($db) {
        parent::__construct($db, 'participant');
    }

    protected function base_query(&$params, array $conditions=[], array $order=[]) {
        if (!$this->table)
            throw new RuntimeException(get_class($this) . '::$table is not set');

        $sub_query_1 = 'SELECT * FROM `' . $this->table . '`';

        if (!empty($conditions))
            $sub_query_1 .= ' ' . $this->format_conditions($conditions, $params);

        if (!empty($order)) {
            $atoms = [];
            foreach ($order as $field)
                $atoms[] = $field[0] === '-' ? sprintf('%s DESC', substr($field, 1)) : $field;
            $sub_query_1 .= ' ORDER BY ' . implode(',', $atoms);
        }

        // Non ckeck-in comments
        $sub_query_2 = "SELECT participant_id id
                              ,count(*) comments
                          FROM `comment`
                         WHERE 1=1
                           AND type NOT LIKE 'Check%'
                         GROUP BY participant_id";

        // Last check-in
        $sub_query_3 = "SELECT cc.participant_id id
                              ,max(cc.timestamp) last_checkin
                          FROM `comment` cc
                         WHERE 1=1
                           AND type LIKE 'Check-in'
                         GROUP BY participant_id";

        // Last check-out
        $sub_query_4 = "SELECT cc.participant_id id
                              ,max(cc.timestamp) last_checkout
                          FROM `comment` cc
                         WHERE 1=1
                           AND type LIKE 'Check-out'
                         GROUP BY participant_id";

        return sprintf(
            "SELECT t1.*
                   ,t2.comments
                   ,t3.last_checkin
                   ,t4.last_checkout
               FROM (%s) t1
                    LEFT JOIN (%s) t2 ON t1.id = t2.id
                    LEFT JOIN (%s) t3 ON t1.id = t3.id
                    LEFT JOIN (%s) t4 ON t1.id = t4.id",
            $sub_query_1,
            $sub_query_2,
            $sub_query_3,
            $sub_query_4
        );
    }

    /**
     * Select data from table
     */
    public function get(array $conditions=[], array $order=[], $get_first=false) {
        $params = [];

        $query = $this->base_query($params, $conditions, $order);

        if ($get_first)
            return $this->query_first($query, $params);
        return $this->query($query, $params);
    }

    /**
     * Select data from table
     */
    public function get_present(array $conditions=[], array $order=[], $get_first=false) {
        $params = [];

        $query = $this->base_query($params, $conditions, $order);

        $query .= ' WHERE t3.last_checkin OR t3.last_checkin > t4.last_checkout';

        if ($get_first)
            return $this->query_first($query, $params);
        return $this->query($query, $params);
    }

    /**
     * Select data from table
     */
    public function filter_name($name) {
        $query = sprintf(
            "SELECT * 
               FROM `%s`
              WHERE concat(first_name, ' ', surname) LIKE :name",
            $this->table
        );

        return $this->query($query, [':name' => '%' . $name . '%']);
    }

    /**
     * Count rows in table
     */
    public function count(array $conditions=[]) {
        if (!$this->table)
            throw new RuntimeException(get_class($this) . '::$table is not set');

        $query = 'SELECT COUNT(*) cnt FROM `' . $this->table . '`';
        $params = [];

        if (!empty($conditions))
            $query .= ' ' . $this->format_conditions($conditions, $params);

        return $this->query_first($query, $params)['cnt'];
    }
}
