<?php
require_once 'include/init.php';
require_once 'include/form.php';

/** Renders and processes the camp registration form */
class ScannerForm extends Bootstrap3Form
{
    public function __construct($name){
        $model = get_model('Comment');
        $type_options = [['Select comment type', ['selected', 'disabled']]];
        $type_options = array_merge($type_options, $model::$type_options);

        $fields = [
            'mode'                => new CheckBoxField ('Strict mode', true),
            'type'                => new SelectField   ('Comment type', $type_options),
            'comment'             => new TextAreaField ('Comment', true, ['maxlength' => 4096])
        ];

        $fields['mode']->value = 'strict';
        return parent::__construct($name, $fields);
    }
}
